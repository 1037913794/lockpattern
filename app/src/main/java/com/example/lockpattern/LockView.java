package com.example.lockpattern;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2018/5/30.
 */

public class LockView extends View {

    private static final int DEFAULT_NORMAL_COLOR = 0xee776666;
    private static final int DEFAULT_MOVE_COLOR = 0xee0000ff;
    private static final int DEFAULT_OVER_COLOR = 0xee00ff00;
    private static final int DEFAULT_ROW_COUNT = 3;

    private static final int STATE_NORMAL = 0;
    private static final int STATE_MOVE = 1;
    private static final int STATE_OVER = 2;


    private int normalColor; // 无滑动默认颜色
    private int moveColor;   // 滑动选中颜色
    private int overColor;  // 错误颜色

    public int mWidth;    // 控件宽度

    public int mHeight;   // 控件高度

    private float radius;    // 外圆半径

    private int rowCount;

    private long preTimestamp;

    private int isInCircle = 0;

    private PointF[] points;   // 一维数组记录所有圆点的坐标点

    private Paint innerCirclePaint; // 内圆画笔

    private Paint outerCirclePaint; // 外圆画笔

    private SparseIntArray stateSparseArray;

    private List<PointF> selectedList = new ArrayList<>();

    private Path linePath = new Path();    // 手指移动的路径

    private Paint linePaint;

    private Timer timer;

    public LockView(Context context) {
        this(context, null);
    }

    public LockView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        readAttrs(context, attrs);
        init();
    }

    private void readAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LockView);
        normalColor = typedArray.getColor(R.styleable.LockView_normalColor, DEFAULT_NORMAL_COLOR);
        moveColor = typedArray.getColor(R.styleable.LockView_moveColor, DEFAULT_MOVE_COLOR);
        overColor = typedArray.getColor(R.styleable.LockView_errorColor, DEFAULT_OVER_COLOR);
        rowCount = typedArray.getInteger(R.styleable.LockView_rowCount, DEFAULT_ROW_COUNT);
        typedArray.recycle();
    }

    private void init() {
        stateSparseArray = new SparseIntArray(rowCount * rowCount);
        points = new PointF[rowCount * rowCount];

        innerCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        innerCirclePaint.setStyle(Paint.Style.FILL);

        outerCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        outerCirclePaint.setStyle(Paint.Style.FILL);

        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeCap(Paint.Cap.ROUND);
        linePaint.setStrokeJoin(Paint.Join.ROUND);
        linePaint.setStrokeWidth(30);
        linePaint.setColor(moveColor);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        // 外圆半径 = 相邻外圆之间间距 = 2倍内圆半径
        radius = Math.min(w, h) / (2 * rowCount + rowCount - 1) * 1.0f;
        // 各个圆设置坐标点
        for (int i = 0; i < rowCount * rowCount; i++) {
            points[i] = new PointF(0, 0);
            points[i].set((i % rowCount * 3 + 1) * radius, (i / rowCount * 3 + 1) * radius);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getSize(widthMeasureSpec);
        int height = getSize(heightMeasureSpec);
        setMeasuredDimension(width, height);
    }

    private int getSize(int measureSpec) {
        int mode = MeasureSpec.getMode(measureSpec);
        int size = MeasureSpec.getSize(measureSpec);
        if (mode == MeasureSpec.EXACTLY) {
            return size;
        } else if (mode == MeasureSpec.AT_MOST) {
            return Math.min(size, dp2Px(600));
        }
        return dp2Px(600);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCircle(canvas);
        drawLinePath(canvas);
    }

    private void drawCircle(Canvas canvas) {
        // 依次从索引 0 到索引 8，根据不同状态绘制圆点
        for (int index = 0; index < rowCount * rowCount; index++) {
            int state = stateSparseArray.get(index);
            switch (state) {
                case STATE_NORMAL:
                    innerCirclePaint.setColor(normalColor);
                    outerCirclePaint.setColor(normalColor & 0x66ffffff);
                    break;
                case STATE_MOVE:
                    innerCirclePaint.setColor(moveColor);
                    outerCirclePaint.setColor(moveColor & 0x66ffffff);
                    break;
                case STATE_OVER:
                    innerCirclePaint.setColor(overColor);
                    outerCirclePaint.setColor(overColor & 0x66ffffff);
                    break;
            }
            canvas.drawCircle(points[index].x, points[index].y, radius, outerCirclePaint);
            canvas.drawCircle(points[index].x, points[index].y, radius / 2f, innerCirclePaint);
        }
    }

    /**
     * 绘制选中点之间相连的路径
     *
     * @param canvas
     */
    private void drawLinePath(Canvas canvas) {
        // 重置linePath
        linePath.reset();
        // 选中点个数大于 0 时，才绘制连接线段
        if (selectedList.size() > 0) {
            // 起点移动到按下点位置
            linePath.moveTo(selectedList.get(0).x, selectedList.get(0).y);
            for (int i = 1; i < selectedList.size(); i++) {
                linePath.lineTo(selectedList.get(i).x, selectedList.get(i).y);
            }
            // 手指抬起时，touchPoint设置为null,使得已经绘制游离的路径，消失掉，
            if (touchPoint != null) {
                linePath.lineTo(touchPoint.x, touchPoint.y);
            }
            canvas.drawPath(linePath, linePaint);
        }
    }

    private PointF touchPoint;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                reset();
                listener.onStart();
                initStroke(event);
                break;
            case MotionEvent.ACTION_MOVE:
                if (touchPoint == null) {
                    touchPoint = new PointF(event.getX(), event.getY());
                } else {
                    touchPoint.set(event.getX(), event.getY());
                }
                boolean flag = true;
                for (int i = 0; i < rowCount * rowCount; i++) {
                    if (getDistance(touchPoint, points[i]) < radius) {
                        flag = false;
                        stateSparseArray.put(i, STATE_MOVE);
                        if (!selectedList.contains(points[i])) {
                            selectedList.add(points[i]);
                            isInCircle = i + 1;
                        }
                        break;
                    }
                }
                if (flag) isInCircle = 0;
                updateStroke(event);
                break;
            case MotionEvent.ACTION_UP:
                for (int i = 0; i < stateSparseArray.size(); i++) {
                    int index = stateSparseArray.keyAt(i);
                    stateSparseArray.put(index, STATE_OVER);
                }
                linePaint.setColor(overColor);
                if (listener != null) {
                    listener.onComplete();
                }
                touchPoint = null;
                if (timer == null) {
                    timer = new Timer();
                }
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        linePath.reset();
                        linePaint.setColor(0xee0000ff);
                        selectedList.clear();
                        stateSparseArray.clear();
                        postInvalidate();
                    }
                }, 1000);
                break;
        }
        invalidate();
        return true;
    }

    /**
     * 清除绘制图案的条件，当触发 invalidate() 时将清空图案
     */
    private void reset() {
        touchPoint = null;
        linePath.reset();
        linePaint.setColor(0xee0000ff);
        selectedList.clear();
        stateSparseArray.clear();
//        PointSample.pointList.clear();
    }

    private void initStroke(MotionEvent event) {
        touchPoint = new PointF(event.getX(), event.getY());
        preTimestamp = event.getEventTime();
        isInCircle = 0;
        for (int i = 0; i < rowCount * rowCount; i++) {
            if (getDistance(touchPoint, points[i]) < radius) {
                isInCircle = i + 1;
                break;
            }
        }
//        PointSample p = new PointSample(event.getX(), event.getY(), 0, 0, 0, 0, event.getPressure(), event.getSize(), event.getEventTime(), 0, isInCircle);
//        PointSample.pointList.add(p);
        MainActivity.addPoint(event.getX(), event.getY(), event.getPressure(), event.getSize(), (int) (event.getEventTime() - preTimestamp), isInCircle);
        //updater.updateUI(p);
    }

    private void updateStroke(MotionEvent event) {
//        float duration = (event.getEventTime() - prevTimestamp) / 1000.0f; //Unit: s
//        PointSample lastPoint = PointSample.pointList.get(PointSample.pointList.size() - 1);
//        float coordX = event.getX();
//        float coordY = event.getY();
//        float deltaCoordX = coordX - lastPoint.coordX;
//        float deltaCoordY = coordY - lastPoint.coordY;
//        float velocityX = deltaCoordX / duration;
//        float velocityY = deltaCoordY / duration;
//        float deltaVelocityX = velocityX - lastPoint.velocityX;
//        float deltaVelocityY = velocityY - lastPoint.velocityY;
//
//        float accelerationX = deltaVelocityX / duration;
//        float accelerationY = deltaVelocityY / duration;
//        float direction;
//        if (deltaCoordX == 0) {
//            if (deltaCoordY == 0) direction = 0;
//            else if (deltaCoordY > 0) direction = (float) Math.PI / 2;
//            else direction = (float) -Math.PI / 2;
//        } else
//            direction = deltaCoordX > 0 ? (float) Math.atan(deltaCoordY / deltaCoordX) : (float) -Math.atan(deltaCoordY / deltaCoordX);
//        float velocity = (float) Math.sqrt(Math.pow(deltaCoordX, 2) + Math.pow(deltaCoordY, 2)) / duration;
////        double a = velocityX * accelerationY - accelerationX * velocityY;
////        double b = Math.pow(Math.pow(velocityX, 2) + Math.pow(velocityY, 2), 1.5);
////        float curvature = (float) Math.abs(a / b);
//        PointSample point = new PointSample(coordX, coordY, velocityX, velocityY, velocity, direction, event.getPressure(), event.getSize(), event.getEventTime(), duration, isInCircle);
//        PointSample.pointList.add(point);
//        prevTimestamp = event.getEventTime();
        MainActivity.addPoint(event.getX(), event.getY(), event.getPressure(), event.getSize(), (int) (event.getEventTime() - preTimestamp), isInCircle);
        //updater.updateUI(point);
    }

    public void onStop() {
        timer.cancel();
    }


    public interface OnStartEndEventListener {
        void onStart();

        void onComplete();
    }

    private OnStartEndEventListener listener;

    public void setStartEndEventListener(OnStartEndEventListener listener) {
        this.listener = listener;
    }

    public interface OnDrawMotionListener {
//        void updateUI(PointSample point);
    }

    private OnDrawMotionListener updater;

    public void setOnDrawmotionListener(OnDrawMotionListener updater) {
        this.updater = updater;
    }

    private float getDistance(PointF centerPoint, PointF downPoint) {
        return (float) Math.sqrt(Math.pow(centerPoint.x - downPoint.x, 2) + Math.pow(centerPoint.y - downPoint.y, 2));
    }

    private int dp2Px(int dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, getResources().getDisplayMetrics());
    }

}
