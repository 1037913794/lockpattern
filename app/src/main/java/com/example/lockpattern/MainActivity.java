package com.example.lockpattern;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //original touch point data
    //从刚按下开始，所有点的原始数据
    private static ArrayList<PointFeature> pointList = new ArrayList<PointFeature>();
    private static int height;  //the height of lock pattern
    private static int width;   //the width of lock pattern

    //提取后的数据
//    private ArrayList<ExtractedPoint> pointExtracted = new ArrayList<ExtractedPoint>();
//    private float totalTime;
//    private float averagePressure;
//    private float averageSize;

    private String pattern;
    private String experimenter;
    private int samples = 0;
    private double distance;

    //provide interface for LockView to store data into pointList
    public static void addPoint(float coordinateX, float coordinateY, float pressure, float size, int timestamp, int circle) {
        PointFeature point = new PointFeature((coordinateX - (float) width / 2) / width * 8, (coordinateY - (float) height / 2) / height * 8, pressure, size, timestamp, circle);
        pointList.add(point);
    }

    private void extractFeature() {
        //去除首尾多余的点（不在圆内的点）
        while (pointList.get(0).circle == 0)
            pointList.remove(0);
        while (pointList.get(pointList.size() - 1).circle == 0)
            pointList.remove(pointList.size() - 1);
        int presentCircle = pointList.get(0).circle;
        pattern = String.valueOf(pointList.get(0).circle);
//        ExtractedPoint point = new ExtractedPoint();
//        int timestamp_in = pointList.get(0).timestamp;
//        float coordinateX_in = 0;
//        float coordinateY_in = 0;
//        float direction_sin_in = 0;
//        float direction_cos_in = 0;
//        int timestamp_out;
//        float coordinateX_out;
//        float coordinateY_out;
//        float direction_sin_out;
//        float direction_cos_out;
        for (int i = 0; i < pointList.size(); i++) {
            //进入圆
            if (presentCircle == 0 && pointList.get(i).circle > 0) {
                presentCircle = pointList.get(i).circle;
                pattern = pattern + String.valueOf(pointList.get(i).circle);
//                point.coordinateX /= point.num;
//                point.coordinateY /= point.num;
//                point.pressure /= point.num;
//                point.size /= point.num;
                //计算离开向量的正余弦值
//                float vectorX = pointList.get(i).coordinateX - pointList.get(i - 1).coordinateX;
//                float vectorY = pointList.get(i).coordinateY - pointList.get(i - 1).coordinateY;
//                direction_sin_out = vectorY / (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
//                direction_cos_out = vectorX / (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
                //计算离开向量与圆的交点（近似）的坐标和时间点
//                float OA = distanceToCircle(pointList.get(i - 1).coordinateX, pointList.get(i - 1).coordinateY, pointList.get(i).circle - 1);
//                float OB = distanceToCircle(pointList.get(i).coordinateX, pointList.get(i).coordinateY, pointList.get(i).circle - 1);
//                coordinateX_out = pointList.get(i - 1).coordinateX + vectorX * (1 - OA) / (OB - OA);
//                coordinateY_out = pointList.get(i - 1).coordinateY + vectorY * (1 - OA) / (OB - OA);
//                timestamp_out = pointList.get(i - 1).timestamp + (int) ((pointList.get(i).timestamp - pointList.get(i - 1).timestamp) * (1 - OA) / (OB - OA));
                //计算停留时间并保存
//                point.duration = timestamp_out - timestamp_in;
                //如果不是起始圆
//                if (coordinateX_in != 0 || coordinateY_in != 0) {
                    //计算夹角的正余弦值并保存
//                    point.angle_sin = direction_sin_out * direction_cos_in - direction_cos_out * direction_sin_in;
//                    point.angle_cos = direction_cos_out * direction_cos_in + direction_sin_out * direction_sin_in;
                    //计算弦长与弦向量正余弦值并保存
//                    vectorX = coordinateX_out - coordinateX_in;
//                    vectorY = coordinateY_out - coordinateY_in;
//                    point.distance = (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
//                    point.direction_sin = vectorY / point.distance;
//                    point.direction_cos = vectorX / point.distance;
//                }
                //新建对象并修改进入值
//                pointExtracted.add(point);
//                point = new ExtractedPoint();
//                coordinateX_in = coordinateX_out;
//                coordinateY_in = coordinateY_out;
//                direction_sin_in = direction_sin_out;
//                direction_cos_in = direction_cos_out;
//                timestamp_in = timestamp_out;
            }
            //离开圆
            if (presentCircle > 0 && pointList.get(i).circle == 0) {
                presentCircle = pointList.get(i).circle;
//                point.coordinateX /= point.num;
//                point.coordinateY /= point.num;
//                point.pressure /= point.num;
//                point.size /= point.num;
                //计算离开向量的正余弦值
//                float vectorX = pointList.get(i).coordinateX - pointList.get(i - 1).coordinateX;
//                float vectorY = pointList.get(i).coordinateY - pointList.get(i - 1).coordinateY;
//                direction_sin_out = vectorY / (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
//                direction_cos_out = vectorX / (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
                //计算离开向量与圆的交点（近似）的坐标和时间点
//                float OA = distanceToCircle(pointList.get(i - 1).coordinateX, pointList.get(i - 1).coordinateY, pointList.get(i - 1).circle - 1);
//                float OB = distanceToCircle(pointList.get(i).coordinateX, pointList.get(i).coordinateY, pointList.get(i - 1).circle - 1);
//                coordinateX_out = pointList.get(i - 1).coordinateX + vectorX * (1 - OA) / (OB - OA);
//                coordinateY_out = pointList.get(i - 1).coordinateY + vectorY * (1 - OA) / (OB - OA);
//                timestamp_out = pointList.get(i - 1).timestamp + (int) ((pointList.get(i).timestamp - pointList.get(i - 1).timestamp) * (1 - OA) / (OB - OA));
                //计算停留时间并保存
//                point.duration = timestamp_out - timestamp_in;
                //如果不是起始圆
//                if (coordinateX_in != 0 || coordinateY_in != 0) {
                    //计算夹角的正余弦值并保存
//                    point.angle_sin = direction_sin_out * direction_cos_in - direction_cos_out * direction_sin_in;
//                    point.angle_cos = direction_cos_out * direction_cos_in + direction_sin_out * direction_sin_in;
                    //计算弦长与弦向量正余弦值并保存
//                    vectorX = coordinateX_out - coordinateX_in;
//                    vectorY = coordinateY_out - coordinateY_in;
//                    point.distance = (float) Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
//                    point.direction_sin = vectorY / point.distance;
//                    point.direction_cos = vectorX / point.distance;
//                }
                //新建对象并修改进入值
//                pointExtracted.add(point);
//                point = new ExtractedPoint();
//                coordinateX_in = coordinateX_out;
//                coordinateY_in = coordinateY_out;
//                direction_sin_in = direction_sin_out;
//                direction_cos_in = direction_cos_out;
//                timestamp_in = timestamp_out;
            }
//            point.coordinateX += pointList.get(i).coordinateX;
//            point.coordinateY += pointList.get(i).coordinateY;
//            point.pressure += pointList.get(i).pressure;
//            point.size += pointList.get(i).size;
//            point.num++;
        }
//        point.coordinateX /= point.num;
//        point.coordinateY /= point.num;
//        point.pressure /= point.num;
//        point.size /= point.num;
//        point.duration = pointList.get(pointList.size() - 1).timestamp - timestamp_in;
//        pointExtracted.add(point);
//        totalTime = pointList.get(pointList.size() - 1).timestamp - pointList.get(0).timestamp;
//        averagePressure = 0;
//        averageSize = 0;
//        for (ExtractedPoint p : pointExtracted) {
//            averagePressure += p.pressure;
//            averageSize += p.size;
//        }
//        averagePressure /= pointExtracted.size();
//        averageSize /= pointExtracted.size();
    }

    private void saveOriginalData() {
        try {
            File context = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                    + "/Original/" + pattern + "/" + experimenter + "/");
            //创建目录
            if (!context.exists()) {
                context.mkdirs();
            }
            File file = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                    + "/Original/" + pattern + "/" + experimenter + "/" + String.format("%03d", context.listFiles().length + 1) + ".csv");
            if (file.exists()) file.delete();
            file.createNewFile();
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));
            bw.write("coordinateX,coordinateY,pressure,size,timestamp,circle");
            bw.newLine();
            for (PointFeature p : pointList) {
                bw.write("" + p.coordinateX + "," + p.coordinateY + "," + p.pressure + "," + p.size + "," + p.timestamp + "," + p.circle);
                bw.newLine();
            }
            bw.close();
            samples = context.listFiles().length;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void saveData() {
//        try {
//            File context = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
//                    + "/Extracted/" + pattern + "/" + experimenter + "/");
//            //创建目录
//            if (!context.exists()) {
//                context.mkdirs();
//            }
//            File file = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
//                    + "/Extracted/" + pattern + "/" + experimenter + "/" + String.format("%03d", context.listFiles().length + 1) + ".csv");
//            if (file.exists()) file.delete();
//            file.createNewFile();
//            BufferedWriter bw = new BufferedWriter(new FileWriter(file, false));
//            for (ExtractedPoint p : pointExtracted) {
//                bw.write("" + p.coordinateX);
//                bw.newLine();
//            }
//            for (ExtractedPoint p : pointExtracted) {
//                bw.write("" + p.coordinateY);
//                bw.newLine();
//            }
//            for (ExtractedPoint p : pointExtracted) {
//                bw.write("" + p.duration);
//                bw.newLine();
//            }
////            bw.write("" + totalTime);
////            bw.newLine();
//            for (ExtractedPoint p : pointExtracted) {
//                bw.write("" + p.pressure);
//                bw.newLine();
//            }
////            bw.write("" + averagePressure);
////            bw.newLine();
//            for (ExtractedPoint p : pointExtracted) {
//                bw.write("" + p.size);
//                bw.newLine();
//            }
////            bw.write("" + averageSize);
////            bw.newLine();
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                bw.write("" + pointExtracted.get(i).angle_sin);
//                bw.newLine();
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                bw.write("" + pointExtracted.get(i).angle_cos);
//                bw.newLine();
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                bw.write("" + pointExtracted.get(i).direction_sin);
//                bw.newLine();
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                bw.write("" + pointExtracted.get(i).direction_cos);
//                bw.newLine();
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                bw.write("" + pointExtracted.get(i).distance);
//                bw.newLine();
//            }
//            bw.close();
//            samples = context.listFiles().length;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private void calculateDistance() {
//        File file = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
//                + "/Extracted/" + pattern + "/" + experimenter + "/");
//        if (file.exists() && file.listFiles().length > 0) {
//            int filecount = 10;
//            double[][] array = new double[Math.min(filecount, file.listFiles().length) + 1][pointExtracted.size() * 8 - 7];
//            for (int i = 0; i < Math.min(filecount, file.listFiles().length); i++) {
//                try {
//                    BufferedReader reader = new BufferedReader(new FileReader(file.listFiles()[i + file.listFiles().length - Math.min(filecount, file.listFiles().length)]));
//                    String line = reader.readLine();//第一行信息，为标题信息，不用,如果需要，注释掉
//                    int j = 0;
//                    // line = reader.readLine();
//                    while (line != null) {
//                        array[i][j] = Double.valueOf(line);
//                        j++;
//                        line = reader.readLine();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            int n = 0;
//            for (ExtractedPoint p : pointExtracted) {
//                array[Math.min(filecount, file.listFiles().length)][n] = p.duration;
//                n++;
//            }
//            array[Math.min(filecount, file.listFiles().length)][n] = totalTime;
//            n++;
//            for (ExtractedPoint p : pointExtracted) {
//                array[Math.min(filecount, file.listFiles().length)][n] = p.pressure;
//                n++;
//            }
//            array[Math.min(filecount, file.listFiles().length)][n] = averagePressure;
//            n++;
//            for (ExtractedPoint p : pointExtracted) {
//                array[Math.min(filecount, file.listFiles().length)][n] = p.size;
//                n++;
//            }
//            array[Math.min(filecount, file.listFiles().length)][n] = averageSize;
//            n++;
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                array[Math.min(filecount, file.listFiles().length)][n] = pointExtracted.get(i).angle_sin;
//                n++;
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                array[Math.min(filecount, file.listFiles().length)][n] = pointExtracted.get(i).angle_cos;
//                n++;
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                array[Math.min(filecount, file.listFiles().length)][n] = pointExtracted.get(i).direction_sin;
//                n++;
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                array[Math.min(filecount, file.listFiles().length)][n] = pointExtracted.get(i).direction_cos;
//                n++;
//            }
//            for (int i = 1; i < pointExtracted.size() - 1; i++) {
//                array[Math.min(filecount, file.listFiles().length)][n] = pointExtracted.get(i).distance;
//                n++;
//            }
//            distance = 0.0;
//            for (int j = 0; j < pointExtracted.size() * 8 - 7; j++) {
//                double average = 0.0;
//                for (int i = 0; i < array.length - 1; i++) {
//                    average += array[i][j];
//                }
//                average /= (array.length - 1);
//                double variance = 0.0;
//                for (int i = 0; i < array.length - 1; i++) {
//                    variance += (array[i][j] - average) * (array[i][j] - average);
//                }
//                variance /= (array.length - 1);
//                array[array.length - 1][j] -= average;
//                if (variance > 0) array[array.length - 1][j] /= Math.sqrt(variance);
//                distance += array[array.length - 1][j] * array[array.length - 1][j];
//            }
//            distance = Math.sqrt(distance);
//        }
//    }

    private float distanceToCircle(float coordinateX, float coordinateY, int circle) {
        float centerX = circle % 3 * 3 - 3, centerY = circle / 3 * 3 - 3;
        return (float) Math.sqrt(Math.pow(coordinateX - centerX, 2) + Math.pow(coordinateY - centerY, 2));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LockView lockView = findViewById(R.id.lock_view);

        final EditText experimenterText = findViewById(R.id.experimenter);
        final EditText thresholdText = findViewById(R.id.threshold);

        final Button deleteButton = findViewById(R.id.delete);

        final TextView viewSizeText = findViewById(R.id.view_size);
        final TextView patternText = findViewById(R.id.pattern);
        final TextView samplesText = findViewById(R.id.samples);
        final TextView distanceText = findViewById(R.id.distance);
        final TextView unlockedText = findViewById(R.id.unlocked);

        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE);
        }

        //listen and set the size of the lock_pattern module
        ViewTreeObserver observer = lockView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lockView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                width = lockView.getMeasuredWidth();
                height = lockView.getMeasuredHeight();
                viewSizeText.setText("W:" + width + "\t" + "H:" + height);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File context_e = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                        + "/Extracted/" + pattern + "/" + experimenterText.getText().toString() + "/");
                if (context_e.exists()) {
                    if (context_e.listFiles().length <= 0) context_e.delete();
                    else {
                        File file = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                                + "/Extracted/" + pattern + "/" + experimenterText.getText().toString() + "/" + String.format("%03d", context_e.listFiles().length) + ".csv");
                        if (file.exists()) file.delete();
                        samplesText.setText(String.valueOf(context_e.listFiles().length));
                    }
                }
                File context_o = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                        + "/Original/" + pattern + "/" + experimenterText.getText().toString() + "/");
                if (context_o.exists()) {
                    if (context_o.listFiles().length <= 0) context_o.delete();
                    else {
                        File file = new File(getApplicationContext().getExternalFilesDir(null).getAbsolutePath()
                                + "/Original/" + pattern + "/" + experimenterText.getText().toString() + "/" + String.format("%03d", context_o.listFiles().length) + ".csv");
                        if (file.exists()) file.delete();
                        samplesText.setText(String.valueOf(context_o.listFiles().length));
                    }
                }
            }
        });

        lockView.setStartEndEventListener(new LockView.OnStartEndEventListener() {
            @Override
            public void onStart() {
                pointList.clear();
//                pointExtracted.clear();
            }

            @Override
            public void onComplete() {
                extractFeature();
                patternText.setText(pattern);
                experimenter = experimenterText.getText().toString();
//                calculateDistance();
                saveOriginalData();
//                saveData();
                samplesText.setText(String.valueOf(samples));
                distanceText.setText(String.valueOf(distance));
                if (distance <= Double.valueOf(thresholdText.getText().toString()))
                    unlockedText.setText("Successful!");
                else unlockedText.setText("Failed!");
            }
        });
    }
}

