package com.example.lockpattern;

class PointFeature {
    float coordinateX;
    float coordinateY;
    //    float velocityX;
    //    float velocityY;
    //    float accelerationX;
    //    float accelerationY;
    //    float velocity;     //linear velocity
    //    float direction;    //included angle
    //    float curvature;
    float pressure;
    float size;
    int timestamp;
    //    float duration;
    int circle;

    PointFeature(float coordinateX, float coordinateY, float pressure, float size, int timestamp, int circle) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.pressure = pressure;
        this.size = size;
        this.timestamp = timestamp;
        this.circle = circle;
    }
}
