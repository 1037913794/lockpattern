package com.example.lockpattern;

class ExtractedPoint {
    float coordinateX;
    float coordinateY;
    float duration;
    float pressure;
    float size;
    float distance;
    float direction_sin;
    float direction_cos;
    float angle_sin;
    float angle_cos;
    int num;

    ExtractedPoint() {
        coordinateX = 0;
        coordinateY = 0;
        duration = 0;
        pressure = 0;
        size = 0;
        distance = 0;
        direction_sin = 0;
        direction_cos = 0;
        angle_sin = 0;
        angle_cos = 0;
        num = 0;
    }
}
